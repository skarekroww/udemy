package FromUdemy;

public class SelectionSort {

    public static int[] SelectionSort(int[] array){
        for(int i = 0; i < array.length-1; i++){
            int index = i;

            for(int x = i+1; x < array.length; x++){
                if(array[x] < array[index]){
                    index = x;
                }

            }
            int smol = array[index];
            array[index] = array[i];
            array[i] = smol;

        }
        return array;

    }



    public static void main(String[] args) {
        int[] numbers = {9, 5, 1, 4, 3, 7, 6, 8, 0};

        int[] sorted = SelectionSort(numbers);

        for (int x:
             sorted) {
            System.out.print(x+ " ");
        }


    }
}
