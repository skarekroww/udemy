package FromUdemy;

public class ExtendCompare {
    public static <T extends Comparable<T>> T max(T x, T y, T z){
        T max = x;
        if(y.compareTo(max) > 0){
            max = y;
        }
        if(z.compareTo(max) > 0){
            max = z;
        }

        return max;
    }
    public static void main(String[] args) {
        System.out.printf("max is %d, %d, %d, is %d", 5, 6, 3, max(5, 6, 3));

    }
}
